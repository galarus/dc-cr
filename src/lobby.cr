require "clear"
require "json"

class Post
  include Clear::Model
  column event : String
  column payload : String
  column author : String
  column circle : String?
  column channel : Int32?
  column created_on : String
  column id : Int64, primary: true, presence: false
end

def newPostJson(event : String, author : String = "", payload : String = "", circle : String = "", channel : Int32 = 0)
  {"event" => event, "author" => author, "payload" => payload, "circle" => circle, "created_on" => Time.local.to_s}
end

def fetchPosts(page : Int32, size : Int32)
  init = page * size
  puts "fetching posts from #{init} plus #{size}"
  res = Post.query.order_by("id", "DESC").limit(size).offset(init).to_a.reverse!.to_json
  # puts res
  res
end

class LobbySesh
  def initialize
    @sockets = Hash(HTTP::WebSocket, String).new
  end

  def putSox
    puts @sockets.values
  end

  def delSock(socket : HTTP::WebSocket)
    @sockets.delete(socket)
  end

  def saveMsg(msg : String)
    msgJSON = JSON.parse(msg)
    a_post = Post.new(msgJSON)
    a_post.save!
  end

  def broadcast(msg : String)
    @sockets.keys.each do |a_sock|
      a_sock.send msg
    end
  end

  def broadcastSave(msg : String)
    puts "broadcasting and saving msg #{msg}"
    msgJSON = JSON.parse(msg.dup)
    new_post = newPostJson(msgJSON["event"].as_s, msgJSON["author"].as_s, msgJSON["payload"].as_s).to_json
    saveMsg new_post.dup

    broadcast new_post
  end

  def lobbyHandler(socket : HTTP::WebSocket)
    puts "got connection"
    # Handle incoming message and dispatch it to all connected clients
    socket.on_message do |message|
      puts message
      json_m = JSON.parse(message.dup)
      ev = json_m["event"]

      if ev == "join"
        username : String = json_m["author"].to_json
        @sockets[socket] = username
        puts "adding socket: #{socket}"
        puts "#{username} joined"
        initMsgs = fetchPosts(0, 40)
        sendInitMsgs = newPostJson("joined", username, initMsgs)
        socket.send sendInitMsgs.to_json
      end

      if ev == "fetch"
        page : Int32 = json_m["page"].to_json.to_i
        size : Int32 = json_m["size"].to_json.to_i
        msgs = fetchPosts(page, size)
        sendMsgs = newPostJson("fetched", "", msgs)
        socket.send sendMsgs.to_json
      end

      if ev == "message"
        puts "got a message. sending to the others"
      end

      if ev == "clear"
        Post.query.to_delete.execute
        broadcastMsg = newPostJson("cleared")
        broadcast broadcastMsg.to_json
      end

      if ev == "leave"
        username = json_m["author"].to_s
        delSock socket
        puts "closing socket: #{socket}"
        putSox
      end

      if (ev != "clear" && ev != "fetch")
        broadcastSave message
      end
    end

    # Handle disconnection and clean sockets
    socket.on_close do |_|
      name = @sockets[socket]
      leave_json = newPostJson("leave", name)
      broadcastSave leave_json.to_json
      delSock socket
      puts "Closing Socket: #{socket}"
      putSox
    end
  end
end
