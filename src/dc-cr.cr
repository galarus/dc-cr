require "kemal"
require "kemal-session"
require "clear"
require "json"
require "uuid"
require "uuid/json"
require "./sql/migrate"
require "./lobby"
require "./circle"

serve_static false

class Account
  include Clear::Model
  column user_id : Int32, primary: true, presence: false
  column username : String
  column created_on : String
  column last_login : String?
  column options : String?
end

# hash of UUID => name
online_users = Hash(String, String).new

Kemal::Session.config do |config|
  config.secret = "some_secret"
end

# check if user has sesh
def authorized?(env)
  env.session.string?("user")
end

# enable cors
before_all do |env|
  env.response.headers.add("Access-Control-Allow-Origin", "*")
  env.response.headers["Access-Control-Allow-Methods"] = "GET, HEAD, POST, PUT, OPTIONS"
  env.response.headers["Access-Control-Allow-Headers"] = "Content-Type, Accept, Origin, Authorization"
  env.response.headers["Access-Control-Max-Age"] = "86400"
end


before_get "/get" do |env|
  raise "unauthorized" unless authorized?(env)
end

get "/get" do |env|
  uSeshID = env.session.string("user")
  username = online_users[uSeshID]
  "Value is #{uSeshID} and name is #{username}"
end

options "/auth/login" do |env|
end

post "/auth/login" do |env|
  name = env.params.json["name"].as(String)
  user_res = Account.query.where { (username == name) }.first
  if user_res
    user_res.last_login = Time.local.to_s
    new_id = UUID.random.to_s
    env.session.string("user", new_id)
    online_users[new_id] = name
    "login success"
  else
    "login failure"
  end
end

get "/auth/logout" do |env|
  uSeshID = env.session.string("user")
  #  online_users[uSeshID].delete
  #  env.session.delete
end

get "/auth/check/:name" do |env|
  check_name = env.params.url["name"].as(String)
  env.response.content_type = "application/json"
  q_res = Account.query.where { (username == check_name) }.first
  if q_res
    puts q_res.created_on
    {"created_on": q_res.created_on}.to_json
  else
    {"created_on": nil}.to_json
  end
end

options "/auth/register" do |env|
end

post "/auth/register" do |env|
  name = env.params.json["name"].as(String)
  acc = Account.new({username: name, created_on: Time.local})
  begin
    result = acc.save!
  rescue
    puts "unable to register"
    "unable to register"
  else
    puts "register #{acc.user_id}"
    "#{acc.user_id}"
  end
end

sesh = LobbySesh.new

ws "/lobby" do |socket, context|
  sesh.lobbyHandler socket
end

get "/circle" do |env|
end

Kemal.config.port = 3333
Kemal.run



module Dc::Cr
  VERSION = "0.1.0"

  # TODO: Put your code here
end
