require "clear"
require "json"
require "kemal"

class Circle
  include Clear::Model
  column name : String
  column description : String?
  column author : String
  column options : String?
  column created_on : String
  column id : Int32, primary: true, presence: false
end

# list all rooms
# get "/c" do |env|
# ans = Circle.query.to_a.to_json.to_s
# puts ans
# ans
# end

def newCircleJson(name : String, author : String, description : String = "", options : String = "")
  {"name" => name, "author" => author, "description" => description, "options" => options, "created_on" => Time.local.to_s}
end

def fetchCircles(page : Int32)
  size = 10
  init = page * size
  puts "fetching posts from #{init} plus #{size}"
  res = Circle.query.order_by("id", "DESC").limit(size).offset(init).to_a.reverse!.to_json
  # puts res
  res
end

ws "/c" do |socket|
  puts "got circle connection #{socket}"
  # Handle incoming message and dispatch it to all connected clients
  socket.on_message do |message|
    puts message
    json_m = JSON.parse(message.dup)
    ev = json_m["event"]

    if ev == "join"
      initMsgs = fetchCircles(0)
      puts "sending #{initMsgs}"
      sendInitMsgs = {"event" => "joined", "payload" => initMsgs}
      socket.send sendInitMsgs.to_json
    end

    if ev == "fetch"
      page : Int32 = json_m["page"].to_json.to_i
      msgs = fetchCircles(page)
      puts "sending #{msgs}"
      sendCs = {"event" => "fetched", "payload" => msgs}
      socket.send sendCs.to_json
    end

    if ev == "create"
      puts "got a message. sending to the others"
      payload = json_m["payload"]
      newC = newCircleJson(payload["name"].as_s, payload["author"].as_s, payload["description"].as_s)
      circle = Circle.new(newC.dup)
      circle.save!
      puts "wanna create #{circle.id} #{circle.created_on}"
      sendC = {"event" => "created", "payload" => circle.to_json}
      socket.send sendC.to_json
      # save and send
    end

    if ev == "destroy"
      # Post.query.to_delete.execute
      # broadcastMsg = newPostJson("destroy", name)
      # broadcast broadcastMsg.to_json
      sendD = %({"event": "destroyed", "name": name})
      # delete a room and send
    end
  end

  # Handle disconnection and clean sockets
  socket.on_close do |_|
    puts "Closing Socket: #{socket}"
  end
end

get "/c" do |env|
  Circle.query.to_a.to_json
end

# get circle by name
get "/c/:name" do |env|
  c_name = env.params.url["name"].as(String)
  res = Circle.query.where { (name == c_name) }.to_a.to_json
  puts res
  res
end

# list circles by name of owner
get "/c/byowner/:name" do |env|
  owner = env.params.url["name"].as(String)
  res = Circle.query.where { (author == owner) }.to_a.to_json
  puts res
  res
end

options "/c" do |env|
end
# create room
post "/c" do |env|
  c_name = env.params.json["name"].as(String)
  desc = env.params.json["description"].as(String)
  author = env.params.json["author"].as(String)
  options = env.params.json["options"].as(String)
  new_c = Circle.new({name: c_name, description: desc, author: author, options: options, created_on: Time.local.to_s})
  begin
    result = new_c.save!
  rescue
    puts "unable to create circle"
    "unable to create circle"
  else
    puts "new circle #{new_c.id}"
    "#{new_c.id}"
  end
end

delete "/c/:name" do |env|
  c_name = env.params.url["name"].as(String)
  puts Circle.query.where { (name == c_name) }.to_delete.execute
end

delete "/c" do |env|
  Circle.query.to_delete.execute
end
