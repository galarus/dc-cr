CREATE TABLE IF NOT EXISTS posts (
    id serial PRIMARY KEY,
    event VARCHAR(16) NOT NULL,
    payload TEXT NOT NULL,
    author VARCHAR(32) NOT NULL,
    created_on TIMESTAMP NOT NULL,
    circle VARCHAR(50) NOT NULL,
    channel INT
);