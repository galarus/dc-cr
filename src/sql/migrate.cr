require "clear"
require "dotenv"

class MyMigration1
  include Clear::Migration

  def change(direction)
    direction.up do
      execute(
        File.read("src/sql/create_accounts_table.sql")
      )
      execute(
        File.read("src/sql/create_posts_table.sql")
      )
      execute(
        File.read("src/sql/create_circles_table.sql")
      )
    end

    direction.down do
      execute("DROP TABLE accounts")
      execute("DROP TABLE posts")
      execute("DROP TABLE circles")
    end
  end
end

Dotenv.load
pgHost = ENV["DBHOST"]
Clear::SQL.init("postgres://postgres:docker@#{pgHost}/postgres",
  connection_pool_size: 5)


 Clear::Migration::Manager.instance.apply_all
