CREATE TABLE IF NOT EXISTS accounts (
    user_id serial PRIMARY KEY,
    username VARCHAR(32) NOT NULL,
    created_on TIMESTAMP NOT NULL,
    last_login TIMESTAMP,
    options TEXT 
);