CREATE TABLE IF NOT EXISTS circles (
    id serial PRIMARY KEY,
    name VARCHAR(32) NOT NULL,
    author VARCHAR(32) NOT NULL,
    created_on TIMESTAMP NOT NULL,
    options TEXT,
    description TEXT
);